const parseTor = (value) => {
  return {
    toIDR: (prefix = null, fixed = 0) => {
      // fungsi yang dibuat untuk mempermudah parsing dari format angka/float/string ke format IDR
      if (typeof value === "number") {
        value =
          Number(value) === value && value % 1 !== 0
            ? value.toString().split(".").join(",")
            : value.toString();
      }
      if (fixed > 0) {
        value = parseFloat(value.split(".").join("").split(",").join("."))
          .toFixed(fixed)
          .toString()
          .split(".")
          .join(",");
      }
      let split = value.split(",");
      let remainder = split[0].length % 3;
      let idr = split[0].substr(0, remainder);
      let thousand = split[0].substr(remainder).match(/\d{3}/gi);

      if (thousand) {
        separator = remainder ? "." : "";
        idr += separator + thousand.join(".");
      }

      idr = split[1] != undefined ? idr + "," + split[1] : idr;

      return prefix == null ? idr : `${prefix.trim()} ${idr}`;

      /*
       *     HOW TO USE
       *
       *     (from integer/float)
       *     1. parseTor(1000).toIDR();  --> result = "1.000";
       *     2. parseTor(1000).toIDR(Rp.");  --> result = "Rp. 1.000";
       *     3. parseTor(1000).toIDR(Rp.", 2);  --> result = "Rp. 1.000,00";
       *     4. parseTor(1000.123).toIDR(Rp.", 2);  --> result = "Rp. 1.000,12";
       *     5. parseTor(1000.123).toIDR('', 2);  --> result = "1.000,12";
       *
       *     (form string)
       *     1. parseTor("1000").toIDR();  --> result = "1.000";
       *     2. parseTor("1000").toIDR(Rp.");  --> result = "Rp. 1.000";
       *     3. parseTor("1.000").toIDR(Rp.", 2);  --> result = "Rp. 1.000,00";
       *     4. parseTor("1.000,123").toIDR(Rp.", 2);  --> result = "Rp. 1.000,12";
       *     5. parseTor("1.000,123").toIDR('', 2);  --> result = "1.000,12";
       *
       */
    },
  };
};
